# Maintainer: Bernhard Landauer <bernhard@manjaro.org>
# Maintainer: Mark Wagie <mark at manjaro dot org>

pkgname=matray
pkgver=1.1.4
pkgrel=1
pkgdesc="A Manjaro Linux announcements notification app"
arch=('x86_64' 'aarch64')
url="https://github.com/moson-mo/matray"
license=('BSD-3-Clause AND GPL-3.0-or-later')
depends=('libappindicator-gtk3' 'libgee' 'libsoup' 'noto-fonts')
makedepends=('meson' 'vala' )
conflicts=('mntray')
replaces=('mntray')
source=("$pkgname-$pkgver.tar.gz::$url/archive/refs/tags/v$pkgver.tar.gz")
sha256sums=('b4ed6194741224f8be6b0409ef42d2dc3daa5c57bd61f0a5d51b241c69627143')

build() {
  arch-meson "$pkgname-$pkgver" build
  meson compile -C build
}

package() {

  # binary and translations
  meson install -C build --destdir "$pkgdir"

  cd "$pkgname-$pkgver"

  # license
  install -Dm644 "LICENSE" "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 "resources/LICENSE_ICONS" "$pkgdir/usr/share/licenses/$pkgname/LICENSE_ICONS"

  # application file
  install -Dm644 "resources/misc/$pkgname.desktop" "$pkgdir/usr/share/applications/org.moson.$pkgname.desktop"

  # autostart
  install -Dm644 "resources/misc/autostart.desktop" "$pkgdir/etc/xdg/autostart/org.moson.$pkgname.desktop"

  # icons
  icons="resources/icons/"
  for file in $(find $icons -type f); do
    install -Dm644 ${file} "$pkgdir/usr/share/icons/hicolor/${file#$icons}"
  done
}
